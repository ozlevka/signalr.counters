﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace Microsoft.AspNet.SignalR.StockTicker.SignalR.PerfCounters
{
    public class PerfCountersHub : Hub
    {
        private static PerfCounterTicker s_ticker;
        private static object _locker = new object();
        
        public PerfCountersHub() : base()
        {
             InitTicker();
        }

        private void InitTicker()
        {
            if (s_ticker == null)
            {
                lock (_locker)
                {
                    if (s_ticker == null)
                    {
                        s_ticker = new PerfCounterTicker();
                        s_ticker.Run(GlobalHost.ConnectionManager.GetHubContext<PerfCountersHub>().Clients, TimeSpan.FromSeconds(5));
                    }
                } 
            }
        }
    }
}