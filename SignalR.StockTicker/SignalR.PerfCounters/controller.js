﻿var PerfCountersCtl = function ($scope, perfCountersData) {

    $scope.counterSections = [];
    $scope.visibleCounters = [];
    $scope.currentCounterName = null;
    $scope.lastUpdate = '';
    $scope.visibleData = {};
    $scope.hiddenColumns = {};

    function replaceMonitor(data) {
        if (data) {
            $scope.rawData = data;
            initCounters();
            for (one in data) {
                $scope.counterSections.push(one);
                if (containsSection($scope.visibleCounters, one)) {
                    updateVisibleCounter($scope.rawData[one], one);
                }
            }
        }
        $scope.lastUpdate = new Date();
    };

    $scope.setCounterVisible = function(name) {
        if (name) {
            if (!containsSection($scope.visibleCounters, name)) {
                $scope.visibleCounters.push(name);
                updateVisibleCounter($scope.rawData[name], name);
            }
        }
    };

    $scope.getLastUpdate = function() {
        return $scope.lastUpdate.getHours() + ':' + $scope.lastUpdate.getMinutes() + ':' + $scope.lastUpdate.getSeconds();
    };

    $scope.removeCounter = function (name) {
        var index = $scope.visibleCounters.indexOf(name);
        $scope.visibleCounters.splice(index, 1);

        $scope.visibleData[name] = undefined;
    };

    $scope.hideColumn = function (section, column) {
        var sec = $scope.hiddenColumns[section];
        if (!sec) {
            sec = $scope.hiddenColumns[section] = [];
        }

        sec.push(column);
        updateVisibleCounter($scope.rawData[section], section);
    };

    $scope.showAllColumns = function(name) {
        $scope.hiddenColumns[name] = [];
        updateVisibleCounter($scope.rawData[name], name);
    };

    var updateVisibleCounter = function(counter, name) {
        $scope.visibleData[name] = getUICounter(counter, name);
    };

    var getUICounter = function(counter, name) {
        var counters = [];
        for (nm in counter) {
            var c = counter[nm];
            var cols = [];
            var vals = [];
            for (var i = 0; i < c.length; i++) {
                var hiddenColumns = $scope.hiddenColumns[name];
                if (!hiddenColumns || hiddenColumns.length == 0 || !containsSection(hiddenColumns,c[i].Name)) {
                    cols.push(c[i].Name);
                    vals.push({ value: c[i].Value });
                }
            }
            counters.push({
                name:nm,
                columns: cols,
                values: vals
            });
        }

        return counters;
    };

    var containsSection = function(array, name) {
        if (array) {
            for (var i = 0; i < array.length; i++) {
                if (array[i] == name) {
                    return true;
                }
            }
            return false;
        }
        return false;
    };

    var initCounters = function() {
        $scope.counterSections = [];
        $scope.counters = [];
    };
    

    var ops = perfCountersData();
    ops.setCallbacks(replaceMonitor);
    ops.initializeClient();
};