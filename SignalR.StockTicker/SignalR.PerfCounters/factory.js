﻿var app = angular.module('app', []);
app.value('$', $);
app.factory('perfCountersData', [ '$', '$rootScope', function($, $rootScope) {
    function perfMonitorOperations() {

        var connection;
        var proxy;
        var setData;
        var setCallBacks = function(setDataCallback) {
            setData = setDataCallback;
        };


        var initializeClient = function() {
            connection = $.hubConnection();
            proxy = connection.createHubProxy('perfCountersHub');

            configureProxy();
            start();
        };

        function configureProxy() {
            proxy.on('updateCounters', function (data) {
                $rootScope.$apply(setData(data));
            });
        };

        var start = function() {
            connection.start();
        };

        return {
            initializeClient: initializeClient,
            setCallbacks: setCallBacks
        };

    };
    
    return perfMonitorOperations;
}]);