﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;

namespace Microsoft.AspNet.SignalR.StockTicker.SignalR.PerfCounters
{
    public class PerfCounterTicker
    {
        private readonly IEnumerable<PerformanceCounter> _currentCounters;
        
       public static IEnumerable<PerformanceCounter> GetCounterSpecifiersByCategoryNamePefix(string startedWith)
        {
            if (String.IsNullOrEmpty(startedWith))
                throw new ArgumentNullException("startedWith");

            // Counter specifier pattern

            List<PerformanceCounterCategory> categories = PerformanceCounterCategory.GetCategories()
                .Where(p => p.CategoryName.Contains(startedWith)).ToList();


            // collect counters specifiers
            var countersSpecifiers = new List<PerformanceCounter>();
            foreach (var cat in categories)
            {
                string[] instances = cat.GetInstanceNames();

                if (cat.CategoryType == PerformanceCounterCategoryType.SingleInstance)
                {
                    foreach (PerformanceCounter counter in cat.GetCounters())
                    {
                        countersSpecifiers.Add(counter);
                    }
                }
                else // Dump counters with instances
                {
                    foreach (var instance in instances.Where(x => !string.IsNullOrEmpty(x)))
                    {
                        //if (cat.InstanceExists(instance))
                        {
                            foreach (PerformanceCounter counter in cat.GetCounters(instance))
                            {
                                countersSpecifiers.Add(counter);
                            }
                        }
                    }
                }
            }

            return countersSpecifiers;
        }

        public static dynamic CreateSample(IEnumerable<PerformanceCounter> counters)
        {
            var x = new Dictionary<string, Dictionary<string, List<ExpandoObject>>>();
            foreach (var performanceCounter in counters)
            {
                Dictionary<string, List<ExpandoObject>> counterList;
                if (x.TryGetValue(performanceCounter.CategoryName,out counterList))
                {
                    TryCreateCounterValue(x, counterList, performanceCounter);
                }
                else
                {
                    TryCreateCounterValue(x, null, performanceCounter);
                }
            }

            return x;
        }

        private static void TryCreateCounterValue(Dictionary<string, Dictionary<string, List<ExpandoObject>>> x, Dictionary<string, List<ExpandoObject>> counterList, PerformanceCounter performanceCounter)
        {
            Dictionary<string, List<ExpandoObject>> cntList = counterList;
            if (cntList == null)
            {
                cntList = new Dictionary<string, List<ExpandoObject>>();
                x.Add(performanceCounter.CategoryName,cntList);
            }

            dynamic cntData = new ExpandoObject();

            if (string.IsNullOrEmpty(performanceCounter.InstanceName))
            {
                cntData.Name = "default";
            }
            else
            {
                cntData.Name = performanceCounter.InstanceName;
            }

            cntData.Value = performanceCounter.NextValue();

            List<ExpandoObject> counters;
            if (!cntList.TryGetValue(performanceCounter.CounterName, out counters))
            {
                counters = new List<ExpandoObject>();
                cntList.Add(performanceCounter.CounterName, counters);
            }

            counters.Add(cntData);
        }

        
        public PerfCounterTicker()
        {
            _currentCounters = GetCounterSpecifiersByCategoryNamePefix("ooVoo");
        }
        
        public CancellationTokenSource Run(IHubConnectionContext clients, TimeSpan refreshInterval)
        {
            var token = new CancellationTokenSource();
            Task.Factory.StartNew(
                () =>
                {
                    while (true)
                    {
                        var sample = CreateSample(this._currentCounters);
                        clients.All.updateCounters(sample);
                        Thread.Sleep(refreshInterval);
                    }
                }, token.Token);
            return token;
        }

    }

}