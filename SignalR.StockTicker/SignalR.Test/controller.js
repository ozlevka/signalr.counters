﻿var data = '{"ooVoo:DB VideoServerPackage":[{"Name":"# Avs Update Cache","Value":0},{"Name":"Avs Update Cache/sec","Value":0}],"ooVoo:Send Email Total":[{"Name":"Mail Send Total","Value":0},{"Name":"Mail Failed Total","Value":0},{"Name":"Fake Mail Total","Value":0}],"ooVoo:Nemo:FBConsumer":[{"Name":"# Oracle Queue Faults","Value":0},{"Name":"Oracle Queue Faults /sec","Value":0},{"Name":"# Oracle Queue Consumed","Value":0},{"Name":"Oracle Queue Consumed /sec","Value":0},{"Name":"# Facebook Ids Sended","Value":0},{"Name":"Facebook Ids Sended /sec","Value":0},{"Name":"# Facebook Ids Received","Value":0},{"Name":"Facebook Ids Received /sec","Value":0},{"Name":"# Facebook Ids Failed","Value":0},{"Name":"Facebook Ids Failed /sec","Value":0},{"Name":"# Ids Sended Ldap","Value":0},{"Name":"Ids Sended Ldap/sec","Value":0},{"Name":"# UserInfo Received From Ldap","Value":0},{"Name":"UserInfo Received From Ldap /sec","Value":0},{"Name":"# Moments Saved","Value":0},{"Name":"Moments Saved /sec","Value":0},{"Name":"# Moments Failed","Value":0},{"Name":"Moments Failed /sec","Value":0},{"Name":"# Notifications Sended","Value":0},{"Name":"Notifications Sended /sec","Value":0},{"Name":"# Notifications Failed","Value":0},{"Name":"Notifications Failed /sec","Value":0}],"ooVoo":[{"Name":"queueItemsCount","Value":0},{"Name":"xmppMessagesSent","Value":0}],"ooVoo:Sdk:Api":[{"Name":"# IamAlive requests","Value":0},{"Name":"IamAlive requests per sec","Value":0},{"Name":"# IamAlive requests success","Value":0},{"Name":"# IamAlive failed requests","Value":0},{"Name":"# GetAvs requests","Value":0},{"Name":"GetAvs requests per sec","Value":0},{"Name":"# GetAvs requests success","Value":0},{"Name":"# GetAvs failed requests","Value":0},{"Name":"# JoinToConference requests","Value":0},{"Name":"JoinToConference requests per sec","Value":0},{"Name":"# JoinToConference requests failed","Value":0},{"Name":"# JoinToConference requests success","Value":0}],"ooVoo:Search":[{"Name":"Total searches mail contacts","Value":0},{"Name":"Total searches elastic user data","Value":0},{"Name":"Total fetches email contacts","Value":0},{"Name":"Total searches in facebook","Value":0},{"Name":"Total searches in LDAP","Value":0},{"Name":"Execution Time searches in LDAP","Value":0},{"Name":"Execution Time searches in Facebook","Value":0},{"Name":"Execution Time searches in Elastic data index","Value":0},{"Name":"Execution Time searches in Elastic mail contacts","Value":0},{"Name":"Execution Time searches in Elastic contacts mail","Value":0}]}';

var TestCntl = function($scope) {
    var rawData = JSON.parse(data);

    $scope.counters = [];

    $scope.tryData = function() {
        for (d in rawData) {
            $scope.counters.push(createCounter(d));
        }
    };


    var createCounter = function(cData) {
        var cnt = rawData[cData];
        return {
            name: cData,
            counters:cnt
        };
    };
};
